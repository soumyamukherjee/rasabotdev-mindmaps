# rasabotdev-mindmaps

This Repo consists of RASA Bot Development Mindmaps. 
Anyone who wants to do development with RASA Core / NLU can use these bot development mindmaps, which has steps to :
1. Install RASA on your Machine
2. Rasa Steps to create a simple Bot
3. Rasa Bot Development Guide (Simple mindmap for the complex documentation)
4. coming soon....

Thanks to XMind Trial Version without it, this wont be possible.